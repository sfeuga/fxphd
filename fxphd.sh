#! /usr/bin/env bash

base_url="https://www.fxphd.com/details";

# Extract all details in the current page
extract_details() {
    echo -e "\t Extract details for page $page_number..."
    title=$(grep '<meta name="dc.title" content="' tmp | sed 's|<meta name="dc.title" content="||' | sed 's; | fxphd" />;;');
    author=$(grep '<meta name="author" content="' tmp | sed 's|<meta name="author" content="||' | sed 's|">||');
    course_number=$(grep '<dt>Course Number:</dt><dd> ' tmp | sed 's|<dt>Course Number:</dt><dd> ||' | sed 's|</dd>||');
    tags=$(grep '<div class="fxtag">' tmp | sed 's/<div class="fxtag">//' | sed 's;</div>;;' | tr '\n' ',' | sed 's/,/, /g');
    run_date=$(grep '<dt>Original Run Date:</dt><dd> ' tmp | sed 's|<dt>Original Run Date:</dt><dd> ||' | sed 's|&nbsp;</dd>||');
    duration=$(grep '<dt>Duration:</dt><dd> ' tmp | sed 's|<dt>Duration:</dt><dd> ||' | sed 's|&nbsp;</dd>||');
    thumbnail=$(grep '<meta property="og:image" content="' tmp | sed 's/<meta property="og:image" content="//' | sed 's;" />;;');
    description=$(grep '<meta name="dc.description" content="' tmp | sed 's/<meta name="dc.description" content="//' | sed 's;" />;;');
}


if [[ ! -e "fxphd.md" ]]; then
    echo "| Watched | Title | Course Number | Tags | Run date | Duration | Thumbnail | Description |" >> fxphd.md;
    echo "|:-------:| ----- |:-------------:| ---- |:--------:|:--------:|:---------:| ----------- |" >> fxphd.md;
fi

if [[ -e "last_tracked_page_number.txt" ]]; then
    last_tracked_page_number=$(cat last_tracked_page_number.txt);
    END=$((last_tracked_page_number + 100));
    START=$((last_tracked_page_number + 1 ));

    for page_number in $(seq $START $END); do
        # Check if the page_number number exist
        header=$(curl -s -o /dev/null -D - $base_url/$page_number/ | head -n 1 | awk '{print $2}');
        echo "Get information on page $page_number..."
        # If yes, get page_number content
        if [[ "$header" == "200" ]]; then
            curl -s $base_url/$page_number/ -o tmp;
            extract_details;
            echo "| [ ] | [$title]($base_url/$page_number/) | $author | $course_number | ${tags::-2} | $run_date | $duration | ![Course Thumbnail]($thumbnail) | $description |" >> fxphd.md;
            # Clean temp files
            rm tmp;
        fi

        echo "$page_number" > last_tracked_page_number.txt
    done
else
    for page_number in {1..100}; do
        # Check if the page_number number exist
        header=$(curl -s -o /dev/null -D - $base_url/$page_number/ | head -n 1 | awk '{print $2}');
        echo "Get information on page $page_number..."
        # If yes, get page_number content
        if [[ "$header" == "200" ]]; then
            curl -s $base_url/$page_number/ -o tmp;
            extract_details;
            echo "| [ ] | [$title]($base_url/$page_number/) | $author | $course_number | ${tags::-2} | $run_date | $duration | ![Course Thumbnail]($thumbnail) | $description |" >> fxphd.md;
            # Clean temp files
            rm tmp;
        fi

        echo "$page_number" > last_tracked_page_number.txt
    done
fi
