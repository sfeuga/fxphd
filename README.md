# My fxphd watch list

Delete `fxphd.md` and `last_tracked_page_number.txt` and run `fxphd.sh` to get (_100 by 100 items_) a list of fxphd's courses.
If you want to add more item in the list, just run again the `fxphd.sh` script.
